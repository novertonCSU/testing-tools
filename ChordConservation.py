from computeConvergance import *

def findFloatsInString(string):
    f = []
    for t in string.split():
        try:
            f.append(float(t))
        except ValueError:
            pass
    return f

def extractSums_ChordExact(filename):
    with open(filename) as file:
        sumsFound=False
        sums = []
        difs = []
        rel = []
        for line in file:
            if sumsFound:
                sumsFound=False
                sums.append(findFloatsInString(line)[0]) #Only grabs first value
                difs.append(sums[-1]-sums[0])
                rel.append(difs[-1]/sums[0])
            if "computeSum" in line:
                sumsFound=True
        print("sum:", sums)
        print("dif:", difs)
        print("rel:", rel)
        return [sums, difs, rel]

def extractSums_ChordExactOld(filename):
    with open(filename) as file:
        sumsFound=False
        sums = []
        difs = []
        rel = []
        for line in file:
            if sumsFound:
                sums = findFloatsInString(line) #line.split()[1:]
                sumsFound=False
                print(sums)
            if "computeSum" in line:
                sumsFound=True
        return [sums, difs]

### SET THESE VARIABLES TO POINT TO THE APPROPRIATE CHORD OUTPUT DATA
activeDir = '.'
files = ['p_fixedSmalldt3.out']
#files = ['p_fixedSmalldt.out', 'np_fixedSmalldt.out']
#files = ['p_fixeddt.out', 'np_fixeddt.out']
#files = ['p_cfl1dt.out', 'np_fixeddt.out']

# update file names
ffiles = [activeDir + '/' + f for f in files]
# grab the data and report
for f in ffiles:
    print("\n", f)
    extractSums_ChordExact(f)
